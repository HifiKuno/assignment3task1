/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment2Task1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.*;
import javafx.application.Application;

/*
 The controller class is designed to handle all the interaction between
 the gui and the database. By doing this we can let the GUI just do GUI stuff,
 and the controller can handle interaction with the model. The controller class
 can be modelled to
 */
public class Controller {

    private Connection connection_;
    private Statement statement_;
    private boolean dataBaseInitialized_ = false;
    private String queryBuilder_;
    private boolean isFirstQueryLine_;
    private Application app_;

    //This initializes the controller with the default database connection.
    //Because this is the controller class all exceptions will be handled here.
    public Controller() {
        //app_ = app;
        try {
            // Load the JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            // Connection to a database
            connection_ = DriverManager.getConnection(
                    "jdbc:mysql://45.32.240.64:3306/student", "lenny", "ser.C1n40tp");

            //Create a statement
            statement_ = connection_.createStatement();
            dataBaseInitialized_ = true;
            this.createStudentTableIfDoesNotExist(true);
        } catch (SQLException ex) {
            System.out.println("SQL Exception: " + ex.getMessage());

        } catch (ClassNotFoundException ex) {
            System.out.println("Class not found exception: " + ex.getMessage());
        }
    }

    public Controller(String url, String port, String database, String username,
            String password) {
        try {
            // Load the JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            // Connection to a database
            connection_ = DriverManager.getConnection(
                    "jdbc:mysql://" + url + ":" + port + "/" + database, username,
                    password);

            //Create a statement
            statement_ = connection_.createStatement();
            dataBaseInitialized_ = true;
            this.createStudentTableIfDoesNotExist(true);
        } catch (SQLException ex) {
            System.out.println("SQL Exception: " + ex.getMessage());

        } catch (ClassNotFoundException ex) {
            System.out.println("Class not found exception: " + ex.getMessage());
        }
    }

    //This function is mainly used for testing. If the database does not have a
    //student table it will create one. If doPopulate == true it will also populate
    //the table with 100 students.
    private void createStudentTableIfDoesNotExist(Boolean doPopulate) throws SQLException {
        DatabaseMetaData metadata = connection_.getMetaData();
        ResultSet resultSet;
        boolean exist = false;

        resultSet = metadata.getTables(null, null, "Student", null);
        if (resultSet.next()) {
            exist = true;
        }

        if (exist == false) {
            String createTable = "CREATE TABLE Student"
                    + "("
                    + "StudentID int PRIMARY KEY,"
                    + "Name varchar(255),"
                    + "Asg1 int,"
                    + "Asg2 int,"
                    + "Asg3 int,"
                    + "Exam int"
                    + ");";
            statement_.execute(createTable);

            //Seeing as we have no data for our table let's populate it
            if (doPopulate) {
                int idCount = 1;
                String firstNames[] = {"Sam", "Marsha", "Jonathon", "Rebecca", "Yolanda",
                    "Dave", "Bart", "Rick", "Steve", "Derp"};
                String lastNames[] = {"Smithson", "Johnson", "Spot", "Frankson", "Smith",
                    "Davison", "Phillips", "Tranter", "Clarke", "Hannay"};
                for (int i = 0; i < 10; i++) {
                    for (int j = 0; j < 10; j++) {
                        String insert = "INSERT INTO Student VALUES"
                                + "("
                                + idCount + ","
                                + "\"" + firstNames[i] + " "
                                + lastNames[j] + "\","
                                + (int) (Math.random() * 100) + ","
                                + (int) (Math.random() * 100) + ","
                                + (int) (Math.random() * 100) + ","
                                + (int) (Math.random() * 100)
                                + ");";

                        statement_.execute(insert);
                        idCount++;
                    }
                }
            }
        }
    }

    /*=========================================================
     || TABLE CREATION
     =========================================================*/
    //verifyTableColumns will verify all the columns have proper input.
    public String verifyTableColumns(String name, String type, String size,
            boolean isPrimary, boolean notNull, boolean isUnique) {

        //Make sure the name is not empty and does not contain spaces.
        if (name.contains(" ")) {
            return "Name cannot contain spaces.";
        }

        if (name.isEmpty()) {
            return "Name cannot be empty.";
        }

        //Make sure a type has been selected.
        if (type == null) {
            return "Please select a type";
        }

        //Make sure the size value is numerical only. It does
        //not matter if the size value is blank, as a default value
        //will be added in this case.
        char sizeChars[] = size.toCharArray();
        for (char c : sizeChars) {
            if (!Character.isDigit(c)) {
                return "Size can only contain integers.";
            }
        }

        //Make sure that if primary is selected then Not Null isn't
        if (isPrimary && (notNull)) {
            return "If column is primary, Not Null is implied. Please deselect Not Null or Primary.";
        }

        //Make sure that if primary is selected then isUnique isn't
        if (isPrimary && (isUnique)) {
            return "If column is primary, Unique is implied. Please deselect Unique or Primary.";
        }
        return "passed";
    }

    //verifyTableName will make sure the table name is acceptable. It will also
    //check to make sure the table name hasn't been used before.
    public String verifyTableName(String name) {
        //Verify name doesn't contain illegal chars.
        if (name.isEmpty()) {
            return "Table name cannot be empty.";
        } else if (name.contains(" ")) {
            return "Table name cannot have spaces.";
        } else if (!Character.isAlphabetic(name.charAt(0))) {
            return "Table name must start with a character.";
        }
        //Make sure
        try {
            DatabaseMetaData meta = connection_.getMetaData();
            ResultSet tableNames = meta.getTables(null, null, name, null);
            if (tableNames.next()) {
                return "The table name already exist.";
            }
        } catch (SQLException ex) {
            return "There was a problem trying to verify the table name:\n" + ex.getMessage();
        }
        return "passed";
    }

    public void addTableName(String tableName) {
        queryBuilder_ = "CREATE TABLE " + tableName;
        isFirstQueryLine_ = true;
    }

    public void addTableColumn(String name, String type, String size, boolean primary, boolean notNull, boolean unique) {
        if (isFirstQueryLine_) {
            queryBuilder_ += "(";
            isFirstQueryLine_ = false;
        } else {
            queryBuilder_ += ",";
        }

        queryBuilder_
                += name
                + " "
                + type
                + ((type.equals("varchar") && !(size == null))
                        ? "(" + size + ")"
                        : ((type.equals("varchar") ? "(255)" : "")))
                + " "
                + ((primary) ? "PRIMARY KEY" : "")
                + ((notNull) ? "NOT NULL " : "")
                + ((unique) ? "UNIQUE" : "");

    }

    public String addTableFinalize() {
        queryBuilder_ += ");";
        try {
            statement_.execute(queryBuilder_);
        } catch (SQLException ex) {
            return "There was an error with the SQL: " + ex.getMessage();
        }

        return "passed";
    }

    public String verifyStudentDetails(String id, String name, String asg1,
            String asg2, String asg3, String exam) {
        //Verify inputs
        //Make sure id contatins ints only.
        if (id.isEmpty()) {
            return "Please insert an ID";
        }

        char idChars[] = id.toCharArray();
        for (char c : idChars) {
            if (!Character.isDigit(c)) {
                return "ID can only contain integers.";
            }
        }

        if (name.isEmpty()) {
            return "Name cannot be empty.";
        }

        if (asg1.isEmpty()) {
            return "Please enter grade for Assignment 1";
        }

        char asgChars[] = asg1.toCharArray();
        for (char c : asgChars) {
            if (!Character.isDigit(c)) {
                return "Assignment 1 can only contain integers.";
            }
        }

        int percentageCheck = Integer.parseInt(asg1);
        if (percentageCheck > 100 || percentageCheck < 0) {
            return "Assignment 1 cscore must be between 0 and 100.";
        }

        if (asg2.isEmpty()) {
            return "Please enter grade for Assignment 2";
        }

        asgChars = asg2.toCharArray();
        for (char c : asgChars) {
            if (!Character.isDigit(c)) {
                return "Assignment 2 can only contain integers.";
            }
        }

        percentageCheck = Integer.parseInt(asg2);
        if (percentageCheck > 100 || percentageCheck < 0) {
            return "Assignment 2 score must be between 0 and 100.";
        }

        if (asg3.isEmpty()) {
            return "Please enter grade for Assignment 3";
        }

        asgChars = asg3.toCharArray();
        for (char c : asgChars) {
            if (!Character.isDigit(c)) {
                return "Assignment 3 can only contain integers.";
            }
        }

        percentageCheck = Integer.parseInt(asg3);
        if (percentageCheck > 100 || percentageCheck < 0) {
            return "Assignment 3 score must be between 0 and 100.";
        }

        if (exam.isEmpty()) {
            return "Please enter grade for Exam";
        }

        char examChars[] = exam.toCharArray();
        for (char c : examChars) {
            if (!Character.isDigit(c)) {
                return "Exam grade can only contain integers.";
            }
        }

        percentageCheck = Integer.parseInt(exam);
        if (percentageCheck > 100 || percentageCheck < 0) {
            return "Exam score must be between 0 and 100.";
        }

        //Now all the values have been checked, ensure the student id is unique
        //in the database.
        try {
            ResultSet studentIDs = statement_.executeQuery("SELECT StudentID FROM Student;");
            while (studentIDs.next()) {
                if (studentIDs.getString(1).matches(id)) {
                    throw new IllegalArgumentException("Student ID already exist.");
                }
            }
        } catch (SQLException ex) {
            return "There was a problem trying to verify the student ID:\n" + ex.getMessage();
        } catch (IllegalArgumentException ex) {
            return "There was a problem with the student ID: " + ex.getMessage();
        }
        return "passed";
    }

    public String addStudent(String id, String name, String asg1,
            String asg2, String asg3, String exam) {
        String query = "INSERT INTO Student VALUES("
                + id + ",\""
                + name + "\","
                + asg1 + ","
                + asg2 + ","
                + asg3 + ","
                + exam + ");";
        try {
            statement_.execute(query);
        } catch (SQLException ex) {
            return "There was a problem inserting the student into the table: "
                    + ex.getMessage();
        }
        return "passed";
    }

    public String displayResults(String id, String name, String asg1,
            String asg2, String asg3, String exam) {
        String output = "";
        String query = "SELECT * FROM Student WHERE ";
        if (!id.isEmpty()) {
            query += "StudentID=" + id + ";";
        } else if (!name.isEmpty()) {
            query += "Name LIKE '%" + name + "%';";
        } else if (!asg1.isEmpty()) {
            query += "Asg1=" + asg1 + ";";
        } else if (!asg2.isEmpty()) {
            query += "Asg2=" + asg2 + ";";
        } else if (!asg3.isEmpty()) {
            query += "Asg3=" + asg3 + ";";
        } else if (!exam.isEmpty()) {
            query += "Exam=" + exam + ";";
        } else {
            query += "1;";
        }
        try {
            ResultSet results = statement_.executeQuery(query);
            while (results.next()) {
                output += "ID: " + results.getString(1);
                output += ", Name: " + results.getString(2);
                output += ", Asg1: " + results.getString(3);
                output += "%, Asg2: " + results.getString(4);
                output += "%, Asg3: " + results.getString(5);
                output += "%, Exam: " + results.getString(6);
                int totalScore = (10 * Integer.parseInt(results.getString(3))
                        + 20 * Integer.parseInt(results.getString(4))
                        + 20 * Integer.parseInt(results.getString(5))
                        + 50 * Integer.parseInt(results.getString(6))) / 100;
                output += "%, Total: " + totalScore;
                output += "%\n";
            }
        } catch (SQLException ex) {
            return "There was an error with the SQL: " + ex.getMessage();
        }
        return output;
    }

    public String verifyStudentUpdateDetails(String id, String name, String asg1, String asg2, String asg3, String exam) {

        //Verify inputs
        //Make sure id contatins ints only.
        boolean hasInput = false;

        if (id.isEmpty()) {
            return "Please insert an ID";
        }

        char idChars[] = id.toCharArray();
        for (char c : idChars) {
            if (!Character.isDigit(c)) {
                return "ID can only contain integers.";
            }
        }

        if (!name.isEmpty()) {
            hasInput = true;
        }

        if (!asg1.isEmpty()) {
            hasInput = true;
            char asgChars[] = asg1.toCharArray();
            for (char c : asgChars) {
                if (!Character.isDigit(c)) {
                    return "Assignment 1 can only contain integers.";
                }
            }

            int percentageCheck = Integer.parseInt(asg1);
            if (percentageCheck > 100 || percentageCheck < 0) {
                return "Assignment 1 cscore must be between 0 and 100.";
            }
        }

        if (!asg2.isEmpty()) {
            hasInput = true;
            char asgChars[] = asg2.toCharArray();
            for (char c : asgChars) {
                if (!Character.isDigit(c)) {
                    return "Assignment 2 can only contain integers.";
                }
            }

            int percentageCheck = Integer.parseInt(asg2);
            if (percentageCheck > 100 || percentageCheck < 0) {
                return "Assignment 2 score must be between 0 and 100.";
            }
        }

        if (!asg3.isEmpty()) {
            hasInput = true;
            char asgChars[] = asg3.toCharArray();
            for (char c : asgChars) {
                if (!Character.isDigit(c)) {
                    return "Assignment 3 can only contain integers.";
                }
            }

            int percentageCheck = Integer.parseInt(asg2);
            if (percentageCheck > 100 || percentageCheck < 0) {
                return "Assignment 3 score must be between 0 and 100.";
            }
        }

        if (!exam.isEmpty()) {
            hasInput = true;
            char asgChars[] = exam.toCharArray();
            for (char c : asgChars) {
                if (!Character.isDigit(c)) {
                    return "Exam score can only contain integers.";
                }
            }

            int percentageCheck = Integer.parseInt(exam);
            if (percentageCheck > 100 || percentageCheck < 0) {
                return "Exam score must be between 0 and 100.";
            }
        }

        //Now all the values have been checked, ensure the student id is unique
        //in the database.
        boolean idExist = false;
        try {
            ResultSet studentIDs = statement_.executeQuery("SELECT StudentID FROM Student;");
            while (studentIDs.next()) {
                if (studentIDs.getString(1).matches(id)) {
                    idExist = true;
                    break;
                }
            }
        } catch (SQLException ex) {
            return "There was a problem trying to verify the student ID:\n" + ex.getMessage();
        } catch (IllegalArgumentException ex) {
            return "There was a problem with the student ID: " + ex.getMessage();
        }
        if (!hasInput) {
            return "You must insert at least one field to be changed.";
        }
        if (!idExist) {
            return "The student ID you have entered is not in the database. "
                    + "Cannot update.";
        }
        return "passed";
    }

    public String studentUpdateDetails(String id, String name, String asg1,
            String asg2, String asg3, String exam) {
        boolean isFirst = true;
        String query = "UPDATE Student SET";

        if (!name.isEmpty()) {
            if (!isFirst) {
                query += ",";
            } else {
                isFirst = false;
            }
            query += " Name='" + name + "'";
        }

        if (!asg1.isEmpty()) {
            if (!isFirst) {
                query += ",";
            } else {
                isFirst = false;
            }
            query += " Asg1=" + asg1;
        }

        if (!asg2.isEmpty()) {
            if (!isFirst) {
                query += ",";
            } else {
                isFirst = false;
            }
            query += " Asg2=" + asg2;
        }

        if (!asg3.isEmpty()) {
            if (!isFirst) {
                query += ",";
            } else {
                isFirst = false;
            }
            query += " Asg3=" + asg3;
        }

        if (!exam.isEmpty()) {
            if (!isFirst) {
                query += ",";
            } else {
                isFirst = false;
            }
            query += " Exam=" + exam;
        }

        query += " WHERE StudentID=" + id + ";";
        System.out.println(query);

        try {
            statement_.execute(query);
        } catch (SQLException ex) {
            return "There was a problem updating the student in the table: "
                    + ex.getMessage();
        }
        return "passed";

    }
    
    public String verifyDelete(String id) {
        String result;
        String query = "SELECT Name FROM Student WHERE StudentID=" + id +";";
        try {
            ResultSet rs = statement_.executeQuery(query);
            if (rs.next()){
                result = "Student ID: " + id + ", Name: " + rs.getString(1);
            } else {
                result = "Error: Student ID not found.";
            }
        } catch (SQLException ex) {
            result = "Error: There was an error with the SQL query: " + ex.getMessage();
        }
        return result;
    }

    public String deleteRecord(String id) {
        String result = "passed";
        String query = "DELETE FROM Student WHERE StudentID=" + id +";";
        try {
            statement_.execute(query);
        } catch (SQLException ex) {
            result = "Error: There was an error with the SQL query: " + ex.getMessage();
        }
        return result;
    }
}
