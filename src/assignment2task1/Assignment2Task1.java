package assignment2Task1;

import java.util.ArrayList;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.geometry.Insets;
import javafx.geometry.Pos;

import javafx.scene.Scene;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import javafx.stage.Stage;

/**
 *
 * @author Leonard The Assignment1Task2 class is the View class. It shows the
 * user the interface and allows for interaction. Any input is handled by the
 * controller though.
 */
public class Assignment2Task1 extends Application {

    @Override
    public void start(Stage primaryStage) {
        //Declare a new controller.
        Controller controller;//Initiate controller

        controller = new Controller();

        if (controller == null) {
            System.out.println("Controller failed to initialize. Closing program.");
            System.exit(1);
        }

        Pane inputPane;
        Pane outputPane;
        Pane buttonPane;

        BorderPane mainPain;

        //Declare nodes for inputPane
        TextField tfStudentId;
        TextField tfStudentName;
        TextField tfAsg1Score;
        TextField tfAsg2Score;
        TextField tfAsg3Score;
        TextField tfExamScore;

        Label lblInputTitle;
        Label lblStudentId;
        Label lblStudentName;
        Label lblAss1Score;
        Label lblAss2Score;
        Label lblAss3Score;
        Label lblExamScore;

        GridPane gpBoxInput;

        //Declare nodes for outputPane
        Label lblOutputTitle;
        TextArea taOutput;
        VBox vBoxOutput;

        //Declare nodes for buttonPane
        Button btnAddTable;
        Button btnChangeMysqlServer;
        Button btnAddStudent;
        Button btnRetrieve;
        Button btnUpdateRecord;
        Button btnDeleteRecord;

        HBox hBoxButtons;
        VBox vBoxButtonRows;

        //Initiate and set up input pane.
        inputPane = new Pane();
        inputPane.setPrefSize(600, 300);

        //Initiate lblInputTitle
        lblInputTitle = new Label("Input Fields");
        lblInputTitle.setStyle("-fx-font-size: 20pt");

        //Initiate Nodes that make up Student ID input
        tfStudentId = new TextField();
        tfStudentId.setTooltip(new Tooltip("Please enter the student id here."));
        lblStudentId = new Label("Student Id:");

        //Initiate Nodes that make up Student name input
        tfStudentName = new TextField();
        tfStudentName.setTooltip(new Tooltip("Please enter the student's "
                + "name here."));
        lblStudentName = new Label("Student Name:");

        //Initiate Nodes that make up Assignment 1 score input
        tfAsg1Score = new TextField();
        tfAsg1Score.setTooltip(new Tooltip("Assigment 1 is worth 5% overall."));
        lblAss1Score = new Label("Assignment 1 Score (0 - 100%):");

        //Initiate Nodes that make up Assignment 2 score input
        tfAsg2Score = new TextField();
        tfAsg2Score.setTooltip(new Tooltip("Assigment 2 is worth 20% overall."));
        lblAss2Score = new Label("Assignment 2 Score (0 - 100%):");

        //Initiate Nodes that make up Assignment 3 score input
        tfAsg3Score = new TextField();
        tfAsg3Score.setTooltip(new Tooltip("Assigment 3 is worth 25% overall."));
        lblAss3Score = new Label("Assignment 3 Score (0 - 100%):");

        //Initiate Nodes that make up Exam score input
        tfExamScore = new TextField();
        tfExamScore.setTooltip(new Tooltip("Exam is worth 50% overall."));
        lblExamScore = new Label("Exam Score (0 - 100%):");

        //Initiate gridpane and set up the layout.
        gpBoxInput = new GridPane();
        gpBoxInput.setHgap(15);
        gpBoxInput.setVgap(20);
        gpBoxInput.add(lblInputTitle, 0, 0);
        gpBoxInput.add(lblStudentId, 0, 1);
        gpBoxInput.add(tfStudentId, 1, 1);
        gpBoxInput.add(lblStudentName, 0, 2);
        gpBoxInput.add(tfStudentName, 1, 2);
        gpBoxInput.add(lblAss1Score, 0, 3);
        gpBoxInput.add(tfAsg1Score, 1, 3);
        gpBoxInput.add(lblAss2Score, 0, 4);
        gpBoxInput.add(tfAsg2Score, 1, 4);
        gpBoxInput.add(lblAss3Score, 0, 5);
        gpBoxInput.add(tfAsg3Score, 1, 5);
        gpBoxInput.add(lblExamScore, 0, 6);
        gpBoxInput.add(tfExamScore, 1, 6);

        inputPane.getChildren().add(gpBoxInput);

        //Initiate and set up outputPane
        outputPane = new Pane();

        //Initiate and set up vBoxOutput
        vBoxOutput = new VBox();
        outputPane.getChildren().add(vBoxOutput);

        //Initiate and set up lblTitleOutput
        lblOutputTitle = new Label("Student Records");
        lblOutputTitle.setStyle("-fx-font-size: 20pt");
        vBoxOutput.getChildren().add(lblOutputTitle);

        //Initiate and setup taOutput
        taOutput = new TextArea();
        taOutput.setPrefSize(560, 150);
        taOutput.setTooltip(new Tooltip("Results are displayed here."));
        taOutput.setEditable(false);
        vBoxOutput.getChildren().add(taOutput);

        vBoxButtonRows = new VBox(5);

        //Initiate and set up buttonPane
        buttonPane = new Pane();

        //Initiate hBoxButtons and set up
        hBoxButtons = new HBox(20);
        vBoxButtonRows.getChildren().add(hBoxButtons);
        buttonPane.getChildren().add(vBoxButtonRows);

        //Initiate and set up buttons
        btnAddTable = new Button("Add Table");
        btnAddTable.setMaxWidth(Double.MAX_VALUE);
        btnAddTable.setTooltip(new Tooltip("Press this button to add student details to system."));
        //When btnMark is pressed it will call the controller to add the student.
        //if the fields were valid then the student is added and displayed.
        //If the fields are invalid an error message is shown.
        btnAddTable.setOnAction(e -> {
            Stage addTableStage = new Stage();
            displayTableStage(addTableStage, primaryStage, controller);
        });

        vBoxButtonRows.getChildren().add(btnAddTable);

        //btnAverageMark retrieves the average mark from the controller
        //and displays it in the textArea.
        btnAddStudent = new Button("Add Student");
        btnAddStudent.setMaxWidth(Double.MAX_VALUE);
        btnAddStudent.setTooltip(new Tooltip("Press this button to add a"
                + " new student."));
        btnAddStudent.setOnAction(e -> {
            //Get all the input from the form.
            String id = tfStudentId.getText();
            String name = tfStudentName.getText();
            String asg1 = tfAsg1Score.getText();
            String asg2 = tfAsg2Score.getText();
            String asg3 = tfAsg3Score.getText();
            String exam = tfExamScore.getText();

            //Pass the form input to the verifier. Store result in result
            String result = controller.verifyStudentDetails(id, name, asg1, asg2, asg3, exam);
            //If result didn't pass, display error
            if (!result.equals("passed")) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Invalid Input");
                alert.setHeaderText("There was an error in the student details.");
                alert.setContentText(result);
                alert.showAndWait();
            } else {
                result = controller.addStudent(id, name, asg1, asg2, asg3, exam);
                if (result.equals("passed")){
                    taOutput.setText("Successfully added record:\n"
                            + "\tStudentID " + id 
                            + "\nName: " + name + "\n"
                            + "\tAsg1 Score: " + asg1 + "%\n"
                            + "\tAsg2 Score: " + asg2 + "%\n"
                            + "\tAsg3 Score: " + asg3 + "%\n"
                            + "\tExam Score: " + exam + "%");
                    tfStudentId.setText("");
                    tfStudentName.setText("");
                    tfAsg1Score.setText("");
                    tfAsg2Score.setText("");
                    tfAsg3Score.setText("");
                    tfExamScore.setText("");
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Unknown Error");
                    alert.setHeaderText("There was an error adding the student:");
                    alert.setContentText(result);
                    alert.showAndWait();
                }
            }

        });

        //Note the btnListAll shows all student in the
        //text area. This was not listed in the assignment outline
        //but we were told in a lecture to implement this.
        btnRetrieve = new Button("Retrieve Records");
        btnRetrieve.setMaxWidth(Double.MAX_VALUE);
        btnRetrieve.setTooltip(new Tooltip("Press this button to list all the"
                + " results for all entered students."));
        btnRetrieve.setOnAction(e -> {
            //Get all the input from the form.
            String id = tfStudentId.getText();
            String name = tfStudentName.getText();
            String asg1 = tfAsg1Score.getText();
            String asg2 = tfAsg2Score.getText();
            String asg3 = tfAsg3Score.getText();
            String exam = tfExamScore.getText();

            //Pass the form input to the verifier. Store result in result
            String result = controller.displayResults(id, name, asg1, asg2, asg3, exam);
            if (result.isEmpty()) {
                taOutput.setText("There were no results for those search records.");
            } else {
                taOutput.setText(result);
            }
        });

        btnUpdateRecord = new Button("Update Records");
        btnUpdateRecord.setMaxWidth(Double.MAX_VALUE);
        btnUpdateRecord.setTooltip(new Tooltip("Enter a student ID and then enter"
                + " the fields you wish to update."));
        btnUpdateRecord.setOnAction(e -> {
            String id = tfStudentId.getText();
            String name = tfStudentName.getText();
            String asg1 = tfAsg1Score.getText();
            String asg2 = tfAsg2Score.getText();
            String asg3 = tfAsg3Score.getText();
            String exam = tfExamScore.getText();

            //Pass the form input to the verifier. Store result in result
            String result = controller
                    .verifyStudentUpdateDetails(id, name, asg1, asg2, asg3, exam);
            //Make sure update stats are valid.
            if (!result.equals("passed")) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Invalid Input");
                alert.setHeaderText("There was an error in the update details.");
                alert.setContentText(result);
                alert.showAndWait();
            } else {
                result = controller.studentUpdateDetails(id, name, asg1, asg2, asg3, exam);
                //Make sure there was no problem executing the SQL.
                if (!result.equals("passed")) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Invalid Input");
                    alert.setHeaderText("There was an error in the update details.");
                    alert.setContentText(result);
                    alert.showAndWait();
                } else {
                    String fuc = (name.isEmpty()) ? "test " : "best";
                    taOutput.setText("Successfully updated record:\n"
                            + "\tStudentID " + id 
                            + ((!name.isEmpty()) ? "\nNew Name: " + name : "")
                            + ((!asg1.isEmpty()) ? "\nNew Asg1 Score: " + asg1 + "%" : "")
                            + ((!asg2.isEmpty()) ? "\nNew Asg2 Score: " + asg2 + "%" : "")
                            + ((!asg3.isEmpty()) ? "\nNew Asg3 Score: " + asg3 + "%" : "")
                            + ((!exam.isEmpty()) ? "\nNew Exam Score: " + exam + "%" : "" ));
                    tfStudentId.setText("");
                    tfStudentName.setText("");
                    tfAsg1Score.setText("");
                    tfAsg2Score.setText("");
                    tfAsg3Score.setText("");
                    tfExamScore.setText("");
                }
            }
        });

        btnDeleteRecord = new Button("Delete Records");
        btnDeleteRecord.setMaxWidth(Double.MAX_VALUE);
        btnDeleteRecord.setTooltip(new Tooltip("Enter a student ID you wish to"
                + " delete then press this."));
        btnDeleteRecord.setOnAction(e -> {
            String id = tfStudentId.getText();
            String details = controller.verifyDelete(id);
            if (!details.contains("Error")) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Confirm Deletion");
                    alert.setHeaderText("Are you sure you wish to delete?");
                    alert.setContentText("You are about to delete record: "
                    + details + ". Are you sure you wish to continue?");
                    if (ButtonType.OK == alert.showAndWait().get()){
                        String result = controller.deleteRecord(id);
                        if(result.equals("passed")) {
                            taOutput.setText("Successfully deleted record: " 
                            + details + ".");
                            tfStudentId.setText("");
                            tfStudentName.setText("");
                            tfAsg1Score.setText("");
                            tfAsg2Score.setText("");
                            tfAsg3Score.setText("");
                            tfExamScore.setText("");
                        } else {
                            alert.setTitle("Invalid Input");
                            alert.setHeaderText("There was an error in the update details.");
                            alert.setContentText(result);
                            alert.showAndWait();
                        }
                    }
            } else {    
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Deletion Error");
                alert.setHeaderText("There was an error when trying to delete.");
                alert.setContentText(details);
                alert.showAndWait();
            }
        });

        //Layout setting for the buttons
        hBoxButtons.setSpacing(10);
        hBoxButtons.setPadding(new Insets(0, 5, 10, 5));
        hBoxButtons.getChildren().addAll(btnAddStudent, btnRetrieve,
                btnUpdateRecord, btnDeleteRecord);
        hBoxButtons.setAlignment(Pos.TOP_CENTER);

        //The mainPain holds all the other panes.
        mainPain = new BorderPane();
        mainPain.setPadding(new Insets(20, 0, 20, 20));
        mainPain.setTop(outputPane);
        mainPain.setCenter(inputPane);
        mainPain.setBottom(buttonPane);

        //Initialize and set up scene.
        Scene scene = new Scene(mainPain, 600, 750);

        //Set primaryStage details and show.
        primaryStage.setTitle("Programming in Java 2: Assignment 1 Part 2");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /*
     This display a new windoe which is used for adding a new table to the database.

     */
    public void displayTableStage(Stage table, Stage primary, Controller controller) {

        //This list is used to store the types allowed for a column.
        ObservableList<String> columnTypes
                = FXCollections.observableArrayList(
                        "int",
                        "varchar",
                        "boolean"
                );

        //These are all the labels that will be used.
        Label lblTableName;
        Label lblColumnCount;
        Label lblColumnName;
        Label lblColumnType;
        Label lblColumnSize;
        Label lblColumnPrimary;
        Label lblColumnNotNull;
        Label lblColumnUnique;

        //These buttons will be used to add or remove columns.
        Button btnIncreaseColumns;
        Button btnDecreaseColumns;

        //This is the input for the table name.
        TextField tableName;

        //Because the amount of inputs are dynamic, we store each
        //node in an array.
        ArrayList<Label> lblColumnNumber;

        ArrayList<TextField> columnName;

        ArrayList<ComboBox<String>> columnType;

        ArrayList<TextField> columnSize;

        ArrayList<CheckBox> columnPrimary;

        ArrayList<CheckBox> columnNotNull;

        ArrayList<CheckBox> columnIsUnique;

        //This grid pane will hold all the input items, including the
        //buttons to increase and decrease columns.
        GridPane gpInput;

        lblTableName = new Label("Insert table name");
        lblColumnCount = new Label("Columns: 5");
        lblColumnName = new Label("Name");
        lblColumnType = new Label("Type");
        lblColumnSize = new Label("Size/Legnth");
        lblColumnPrimary = new Label("Primary Key");
        lblColumnNotNull = new Label("Not Null");
        lblColumnUnique = new Label("Is Unique");

        lblColumnNumber = new ArrayList<>();
        columnName = new ArrayList<>();
        columnType = new ArrayList<>();
        columnSize = new ArrayList<>();
        columnPrimary = new ArrayList<>();
        columnNotNull = new ArrayList<>();
        columnIsUnique = new ArrayList<>();

        tableName = new TextField();

        gpInput = new GridPane();
        gpInput.setHgap(15);
        gpInput.setVgap(20);

        btnDecreaseColumns = new Button("-");
        btnDecreaseColumns.setOnAction(e -> {
            if (columnName.size() > 0) {
                int current = columnName.size() - 1;

                lblColumnCount.setText("Columns: " + (current));

                gpInput.getChildren().remove(lblColumnNumber.get(current));
                gpInput.getChildren().remove(columnName.get(current));
                gpInput.getChildren().remove(columnType.get(current));
                gpInput.getChildren().remove(columnSize.get(current));
                gpInput.getChildren().remove(columnPrimary.get(current));
                gpInput.getChildren().remove(columnNotNull.get(current));
                gpInput.getChildren().remove(columnIsUnique.get(current));

                lblColumnNumber.remove(current);
                columnName.remove(current);
                columnType.remove(current);
                columnSize.remove(current);
                columnPrimary.remove(current);
                columnNotNull.remove(current);
                columnIsUnique.remove(current);
            }
        });

        btnIncreaseColumns = new Button("+");
        btnIncreaseColumns.setOnAction(e -> {
            int current = columnName.size();

            lblColumnCount.setText("Columns: " + (current + 1));

            lblColumnNumber.add(new Label("Column " + (current + 1) + ":"));
            columnName.add(new TextField());
            columnType.add(new ComboBox(columnTypes));
            columnSize.add(new TextField());
            columnPrimary.add(new CheckBox("Primary"));
            columnNotNull.add(new CheckBox("Not Null"));
            columnIsUnique.add(new CheckBox("Unique"));

            gpInput.add(lblColumnNumber.get(current), 0, 2 + current);
            gpInput.add(columnName.get(current), 1, 2 + current);
            gpInput.add(columnType.get(current), 2, 2 + current);
            gpInput.add(columnSize.get(current), 3, 2 + current);
            gpInput.add(columnPrimary.get(current), 4, 2 + current);
            gpInput.add(columnNotNull.get(current), 5, 2 + current);
            gpInput.add(columnIsUnique.get(current), 6, 2 + current);
        });

        HBox columnButtons = new HBox();
        columnButtons.getChildren().add(btnDecreaseColumns);
        columnButtons.getChildren().add(btnIncreaseColumns);

        gpInput.add(lblTableName, 0, 0);
        gpInput.add(tableName, 1, 0);
        gpInput.add(lblColumnCount, 2, 0);
        gpInput.add(columnButtons, 3, 0);
        gpInput.add(lblColumnName, 1, 1);
        gpInput.add(lblColumnType, 2, 1);
        gpInput.add(lblColumnSize, 3, 1);
        gpInput.add(lblColumnPrimary, 4, 1);
        gpInput.add(lblColumnNotNull, 5, 1);
        gpInput.add(lblColumnUnique, 6, 1);

        for (int i = 0; i < 5; i++) {
            lblColumnNumber.add(new Label("Column " + (i + 1) + ":"));
            columnName.add(new TextField());
            columnType.add(new ComboBox(columnTypes));
            columnSize.add(new TextField());
            columnPrimary.add(new CheckBox("Primary"));
            columnNotNull.add(new CheckBox("Not Null"));
            columnIsUnique.add(new CheckBox("Unique"));

            gpInput.add(lblColumnNumber.get(i), 0, 2 + i);
            gpInput.add(columnName.get(i), 1, 2 + i);
            gpInput.add(columnType.get(i), 2, 2 + i);
            gpInput.add(columnSize.get(i), 3, 2 + i);
            gpInput.add(columnPrimary.get(i), 4, 2 + i);
            gpInput.add(columnNotNull.get(i), 5, 2 + i);
            gpInput.add(columnIsUnique.get(i), 6, 2 + i);
        }

        Button btnAdd = new Button("Add Table");
        btnAdd.setOnAction(e -> {
            int columns = columnName.size();
            boolean hasPassed = true;
            for (int i = 0; i < columns; i++) {
                String name = columnName.get(i).getText();
                String type = columnType.get(i).getValue();
                String size = columnSize.get(i).getText();
                boolean isPrimary = columnPrimary.get(i).isSelected();
                boolean notNull = columnNotNull.get(i).isSelected();
                boolean isUnique = columnIsUnique.get(i).isSelected();

                String result = controller.verifyTableColumns(name, type, size,
                        isPrimary, notNull, isUnique);
                if (!result.matches("passed")) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Invalid Input");
                    alert.setHeaderText("There was an error in column: " + (i + 1));
                    alert.setContentText(result);
                    alert.showAndWait();
                    hasPassed = false;
                    break;
                }
            }
            if (hasPassed) {
                String result = controller.verifyTableName(tableName.getText());
                if (!result.matches("passed")) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Invalid Input");
                    alert.setHeaderText("There was an error with the table name.");
                    alert.setContentText(result);
                    alert.showAndWait();
                    hasPassed = false;
                }
            }

            if (hasPassed) {
                //We'll create the table SQL in three steps. First we'll
                //send over the table name.
                controller.addTableName(tableName.getText());

                //Then we will add each column.
                for (int i = 0; i < columnName.size(); i++) {
                    String name = columnName.get(i).getText();
                    String type = columnType.get(i).getValue();
                    String size = columnSize.get(i).getText();
                    boolean isPrimary = columnPrimary.get(i).isSelected();
                    boolean notNull = columnNotNull.get(i).isSelected();
                    boolean isUnique = columnIsUnique.get(i).isSelected();
                    controller.addTableColumn(name, type, size, isPrimary, notNull, isUnique);
                }

                //We finalize the table and send the query away.
                String result = controller.addTableFinalize();
                System.out.println(result);
                if (result.equals("passed")) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Table Success");
                    alert.setContentText("The table was successfully created.");
                    alert.showAndWait()
                            .filter(response -> response == ButtonType.OK)
                            .ifPresent(response -> closeTable(table, primary));
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Table Creation Error");
                    alert.setContentText("There was an error when trying to create"
                            + " the table. Please try again.");
                    alert.showAndWait()
                            .filter(response -> response == ButtonType.OK);
                }
            }
        });

        Button btnCancel = new Button("Cancel");
        btnCancel.setOnAction(e -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm Cancel");
            alert.setHeaderText("Are you sure you wish to cancel?");
            alert.setContentText("If you cancel now any unsaved chages will be lost."
                    + " Are you sure you wish to continue?");
            alert.showAndWait()
                    .filter(response -> response == ButtonType.OK)
                    .ifPresent(response -> closeTable(table, primary));
        });

        HBox buttons = new HBox();
        buttons.getChildren().add(btnAdd);
        buttons.getChildren().add(btnCancel);

        VBox vbox = new VBox();
        vbox.setVisible(true);
        vbox.getChildren().add(gpInput);
        vbox.getChildren().add(buttons);

        ScrollPane spMain = new ScrollPane();
        spMain.setContent(vbox);
        Scene scene = new Scene(spMain, 1000, 500);
        table.setScene(scene);
        table.show();
        primary.hide();
    }

    private void closeTable(Stage table, Stage primary) {
        table.hide();
        table = null;
        primary.show();
    }

    public void displayAlert(String alertTitle, String alertHeader, String alertMessage,
            Alert.AlertType type) {

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(alertTitle);
        alert.setHeaderText(alertHeader);
        alert.setContentText(alertMessage);
        alert.showAndWait();

    }

}
